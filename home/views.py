from django.shortcuts import render
from .forms import schedule_form
from django.shortcuts import redirect
from django.utils import timezone
from . import forms
from .models import Schedule

# Create your views here.

def index(request):
	return render(request, "index.html")

def photo(request):
	return render(request, "photo.html")

def guestform(request):
	return render(request, "guestform.html")

def myfriend(request):
	return render(request, "myfriend.html")

def schedule(request):
	schedules = Schedule.objects.all().order_by('date')
	return render(request, 'schedule.html', {'schedules' : schedules})

def schedule_add(request):
	if request.method == "POST":
		form = schedule_form(request.POST)
		if form.is_valid():
			form.save()
			return redirect('schedule')
	else:
			form = forms.schedule_form()
	return render(request, 'schedule_add.html', {'form': form})

def deleteAll(request):
	Schedule.objects.all().delete()
	response = {}
	return render(request, 'schedule.html', response)
