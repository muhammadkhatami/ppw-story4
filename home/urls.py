from django.urls import path
from home import views

urlpatterns = [
	path('index/', views.index, name = 'index'),
	path('photo/', views.photo, name = 'photo'),
	path('guestform/', views.guestform, name = 'guestform'),
	path('myfriend/', views.myfriend, name = 'myfriend'),
	path('schedule/', views.schedule, name='schedule'),
	path('schedule_add/', views.schedule_add, name='schedule_add'),
	path('deleteAll/', views.deleteAll, name='deleteAll')
]
